import React, {useState} from "react";
import Styles from "../App.module.css"
const TransActionForm = ({addTransaction,setIsShow}) => {
    const [formValues, setFormValues] = useState({type: "expense", amount: 0, desc: ""});


    const changeHandler = (e) => {
        setFormValues({...formValues, [e.target.name]: e.target.value});

    };
    const submitHandler = (e) => {
        e.preventDefault();
        addTransaction(formValues);
        setIsShow(!setIsShow)
    };
    return (
        <div>
            <form onSubmit={submitHandler}>
                <input placeholder="Description" type="text" name="desc" value={formValues.desc} onChange={changeHandler}/>
                <input placeholder="Amount" type="number" name="amount" value={formValues.amount} onChange={changeHandler}/>
                <div className={Styles.radio}>
                    <input id="expense" type="radio" value="expense" checked={formValues.type === "expense"} name="type"
                           onChange={changeHandler}/>
                    <label htmlFor="expense">Expense</label>
                    <input id="income" type="radio" value="income" checked={formValues.type === "income"} name="type"
                           onChange={changeHandler}/>
                    <label htmlFor="income">Income</label>
                </div>
                <button className={`${Styles.btn}  ${Styles.primery}`} type="submit">Add transaction</button>
            </form>
        </div>
    )
};
export default TransActionForm;