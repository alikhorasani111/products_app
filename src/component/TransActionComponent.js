import Styles from '../App.module.css'
import React, {useState, useEffect} from 'react'

const TransActionComponent = ({transAction}) => {
    const [searchItem, setSearchItem] = useState('');
    const [filteredTnx, setFilteredTnx] = useState(transAction);

    const changeHandler = (e) => {
        setSearchItem(e.target.value);
        filterTransaction(e.target.value);
    };

    const filterTransaction = (search) => {
        if (!search || search === "") {
            setFilteredTnx(transAction);
            return;
        }
        const filtered = transAction.filter(t => t.desc.toLowerCase().includes(search.toLowerCase()));
        setFilteredTnx(filtered)
    };

    useEffect(() => {
        filterTransaction(searchItem)
    }, [transAction]);

    return (
        <section>
            <input placeholder="search" className={Styles.input} type="text" value={searchItem} onChange={changeHandler}/>

            {filteredTnx.length ? filteredTnx.map((trans) => {
                return <div key={trans.id} className={Styles.transaction}
                            style={{borderRight: trans.type === "expense" && "4px solid red"}}>
                    <span>{trans.desc}</span>
                    <span> $ {trans.amount}</span>
                </div>
            }) : "Add some expense"}
        </section>
    )
};
export default TransActionComponent;