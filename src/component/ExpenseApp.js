import React, {useState, useEffect} from "react";
import Styles from '../App.module.css';
import TransActionComponent from "./TransActionComponent";
import OverViewComponent from "./OverViewComponent";

const ExpenseApp = () => {
    const [expense, setExpense] = useState(0);
    const [income, setIncome] = useState(0);
    const [transAction, setTransAction] = useState([]);

    const addTransaction = (formValues) => {
        setTransAction([...transAction, {...formValues, id: Date.now()}]);
    };
    useEffect(() => {
        let exp = 0;
        let inc = 0;
        transAction.forEach((trans) => {
            trans.type === "expense" ? (exp = exp + parseFloat(trans.amount)) : (inc = inc + parseFloat(trans.amount));
        });
        setExpense(exp);
        setIncome(inc);

    }, [transAction]);
    return (
        <section className={Styles.container}>
            <OverViewComponent addTransaction={addTransaction} income={income} expense={expense}/>
            <TransActionComponent transAction={transAction}/>
        </section>
    )
};
export default ExpenseApp;