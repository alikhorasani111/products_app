import Styles from "../App.module.css";
import React, {useState} from "react";
import TransActionForm from "./TransActionForm";

const OverViewComponent = ({income, expense, addTransaction}) => {
    const [isShow, setIsShow] = useState(false);
    return (
        <div>
            <div className={Styles.d_flex}>
                <p>Balance:{income - expense}</p>
                <div>
                    <button className={`${Styles.btn} ${isShow ? Styles.cancel : ""}`}
                            onClick={() => setIsShow(!isShow)}>{isShow ? "Cancel" : "Add"}</button>
                </div>
            </div>
            {isShow ? <TransActionForm addTransaction={addTransaction} setIsShow={setIsShow}/> : ""}

            <div className={Styles.d_flex}>
                <div className={Styles.expense_box}>Expense <span style={{color: "red"}}>{expense} $</span></div>
                <div className={Styles.expense_box}>Income <span>{income} $</span></div>
            </div>
        </div>
    )
};
export default OverViewComponent