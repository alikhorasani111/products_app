import Styles from './App.module.css';
import React from 'react'
import ExpenseApp from "./component/ExpenseApp";

function App() {

    return (
        <div className={Styles.app}>
            <header>
                <h2>Expense Tracker</h2>
            </header>
         <ExpenseApp/>
        </div>

    );
}

export default App;
